#
# Podspec for the public (binary only) version of the Lift SDK
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.name         = "Lift"
  s.version      = "0.4.0"
  s.summary      = "The iOS integration library for Lift Data -- Better Push Notificaions."

  #s.description  = <<-DESC
  #                 A longer description of Lift in Markdown format.
  #                 DESC

  s.homepage     = "http://www.liftdatatech.com"
  s.license      = { :type => "Commericial", :text => "See http://www.liftdatatech.com" }
  s.authors            = { "Lift Data" => "support@liftdatatech.com" }
  
  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #  
  s.platform     = :ios, "6.0"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.source       = { 
    :http => "http://dl.liftdatatech.com.s3.amazonaws.com/lift-ios-#{s.version}.zip", 
    :flatten =>true 
  }

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.source_files = "include/Lift"
  s.header_mappings_dir = "include"
  s.vendored_libraries = "libLift.a"
 
  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.frameworks = "Foundation", 
                 "UIKit", 
                 "CoreTelephony", 
                 "SystemConfiguration"
  s.libraries  = "Lift",
                 "sqlite3"

  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.requires_arc = true

end
